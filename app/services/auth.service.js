import axios from '~/plugins/axios'

class AuthService {
  login(user) {
    return axios
      .post('/merchant-staff/auth', {
        phoneNumber: user.phoneNumber,
        password: user.password,
        osType: 'web',
      })
      .then((response) => {
        return response.data
      })
  }

  logout() {
    // Todo: Handle Logout
  }

  register() {
    // Todo: Handle Register
  }
}

export default new AuthService()
