import Vue from 'nativescript-vue'
import Login from './screens/Login.vue'
import VueDevtools from 'nativescript-vue-devtools'
import Theme from '@nativescript/theme'

if (TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}
import store from './store'

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = TNS_ENV === 'production'

// Disable Dark mode
Theme.setMode(Theme.Light)

new Vue({
  store,
  render: (h) => h('frame', [h(Login)]),
}).$start()
