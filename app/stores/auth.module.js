import AuthService from '~/services/auth.service'
import * as applicationSettings from 'tns-core-modules/application-settings'

const initialState = {
  status: { loggedIn: false },
  user: null,
  token: applicationSettings.getString('token', ''),
}

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }, { user }) {
      return AuthService.login(user).then(
        (user) => {
          commit('loginSuccess', user)
          applicationSettings.setString('token', user.token)
          return Promise.resolve(user)
        },
        (error) => {
          commit('loginFailure')
          return Promise.reject(error.response.data)
        }
      )
    },
    logout({ commit }) {
      applicationSettings.setString('token', '')
      commit('logout')
    },
    // register({ commit }, user) {
    //   return AuthService.register(user).then(
    //     (response) => {
    //       commit('registerSuccess')
    //       return Promise.resolve(response.data)
    //     },
    //     (error) => {
    //       commit('registerFailure')
    //       return Promise.reject(error)
    //     }
    //   )
    // },
  },

  mutations: {
    loginSuccess(state, user) {
      state.status.loggedIn = true
      state.user = user
    },
    loginFailure(state) {
      state.status.loggedIn = false
      state.user = null
    },
    logout(state) {
      state.status.loggedIn = false
      state.user = null
    },
    registerSuccess(state) {
      state.status.loggedIn = false
    },
    registerFailure(state) {
      state.status.loggedIn = false
    },
  },
}
